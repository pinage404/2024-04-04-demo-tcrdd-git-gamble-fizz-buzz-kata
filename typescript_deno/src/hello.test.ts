import { describe, expect, it } from "./dev_deps.ts";

describe("fizzbuzz", () => {
  it("1", () => {
    expect(fizzBuzz(1)).toEqual("1");
  });

  it("fizz", () => {
    expect(fizzBuzz(3)).toEqual("Fizz");
  });

  it("fizz again", () => {
    expect(fizzBuzz(6)).toEqual("Fizz");
  });

  it("4", () => {
    expect(fizzBuzz(4)).toEqual("4");
  });

  it("buzz", () => {
    expect(fizzBuzz(5)).toEqual("Buzz");
  });

  it("fizzbuzz", () => {
    expect(fizzBuzz(15)).toEqual("FizzBuzz");
  });

  it("fizzbuzz again", () => {
    expect(fizzBuzz(30)).toEqual("FizzBuzz");
  });
});
function fizzBuzz(n: number): string {
  if (n % 3 === 0 && n % 5 === 0) return "FizzBuzz";
  if (n % 5 === 0) return "Buzz";
  if (n % 3 === 0) return "Fizz";
  return n.toString();
}
