# Commands

## test

```sh
nix eval .#tests
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## lint

```bash
set -o errexit
set -o nounset
set -o pipefail

statix fix
nix-linter --check=BetaReduction --check=EmptyVariadicParamSet --check=UnneededAntiquote ./**/**.nix
deadnix --no-underscore --fail
```

## format

```sh
nix fmt
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    devbox update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
